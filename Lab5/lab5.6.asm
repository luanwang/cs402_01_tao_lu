.data 0x10000000
user1: .word 0
msg1: .asciiz  "Please enter a integer: "
msg2: .asciiz "If bytes were layed in reverse order the number would be: "
.text
.globl main
main: add $sp, $sp, -4 # save current $ra in stack
      sw $ra, 4($sp)
      li $v0,4
      la $a0,msg1
      syscall

      li $v0,5 # user input an integer
      syscall
      sw $v0,user1 # save the input to user1

      la $a0,user1 # load addrss of user1

      jal Reverse_bytes # jump to Reverse_bytes
      
      move $t0,$v0 # move the savlue $v0 to $t0
      li $v0,4 # print msg2
      la $a0,msg2
      syscall
      
      li $v0,1 # print integer
      move $a0,$t0
      syscall

      lw $ra, 4($sp) # restore $ra
      add $sp, $sp, 4
      jr $ra

Reverse_bytes:    lbu $t1,0($a0) # load unsign byte
                  sll $t1,$t1,24 # left shift 24 bits
                  addu $v0,$t1,$0 # add the value in $t1 to $v0
                  lbu $t1,1($a0) # load unsign byte to $t1
                  sll $t1,$t1,16 # left shift 16 bits
                  addu $v0,$v0,$t1 # add the value in $t1 to $v0
                  lbu $t1,2($a0) # load unsign byte to $t1
                  sll $t1,$t1,8 # left shift 8 bits
                  addu $v0,$v0,$t1 # add the value in $t1 to $v0
                  lbu $t1,3($a0) # load unsign byte in $t1
                  addu $v0,$v0,$t1 # add the value in $1 to $v0
                  jr $ra