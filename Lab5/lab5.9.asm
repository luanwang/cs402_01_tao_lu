# this is a program used to test memory alignment for data 
.data 0x10000000 
.align 0
char1: .byte 'a' # reserve space for a byte 
double1: .double 1.1 # reserve space for a double 
char2: .byte 'b' # b is 0x62 in ASCII 
half1: .half 0x8001 # reserve space for a half-word (2 bytes) 
char3: .byte 'c' # c is 0x63 in ASCII 
word1: .word 0x56789abc # reserve space for a word 
char4: .byte 'd' # d is 0x64 in ASCII

.text 
.globl main 
main:   
        lui $1, 4096
        ori $t0,$1,13
        lwr $t1,0($t0) # load the low 3 bytes of word1 to $t1
        lwl $t2,3($t0) # load the high 1 byte of word1 to $t2
        add $t3,$t1,$t2 # combine $t1 and $t2 to $t3
        jr $ra  # return from main