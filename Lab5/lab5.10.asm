.data 0x10000000
.align 0
ch1: .byte 'a'
word1: .word 0x89abcdef
ch2: .byte 'b'
word2: .word 0

.text
.globl main
main:   lui $t0,0x1000 # load address of word1
        ori $t0,$t0,1
        lwr $t1,0($t0) # load 1. part of word1 to $t1
        lwl $t2,3($t0) # load 2. pard of word2 to $t2
        add $t3,$t1,$t2 # combine $t1 and $t2 to $t3
        lui $t0,0x1000 # load address of word2
        ori $t0,$t0,6 
        swr $t3,0($t0) # save 1. part of $t3 to word2
        swl $t3,3($t0) # save 2. part of $t3 to word2
        jr $ra