#include <iostream>

using std::cin;
using std::cout;

int counter = 0;

int ackermann(int x, int y)
{
    ++counter;
    if (x == 0)
    {
        return y + 1;
    }
    if (y == 0)
    {
        return ackermann(x - 1, 1);
    }
    return ackermann(x - 1, ackermann(x, y - 1));
}

int main()
{
    int x, y;
    cin >> x >> y;
    if (x < 0 || y < 0)
    {
        cout << "plase enter two non-negative integers!\n";
    }
    else
    {
        cout << ackermann(x, y) << '\n';
        cout << "ackermann was called " << counter << " times.\n";
    }
}