.data 0x10000000
msg1: .asciiz "Please enter an integer number:"
msg2: .asciiz "The largest integer is: "

.text
.globl main
main: li $v0,4 # system call to print_str
      la $a0,msg1 # load address of msg1
      syscall

      li $v0, 5 # read integer
      syscall

      addu $t0, $v0, $0 # move input to $t0

      # read the second integer
      li $v0,4 # system call to print_str
      la $a0,msg1 # load address of msg1
      syscall

      li $v0, 5 # read integer
      syscall

      addu $t1, $v0, $0 # move input to $t1

      addu $a0, $t0, $0 # pass the first parameter to $s0 
      addu $a1, $t1, $0 # pass the second parameter to $s1

      add $sp, $sp, -4 # save $ra to stack
      sw $ra, 4($sp)

      jal Largest
      move $t3,$v0
      li $v0, 4 
      la $a0, msg2
      syscall

      li $v0, 1 # print a integer
      add $a0, $t3, $0
      syscall

      lw $ra, 4($sp) # restore $ra
      add $sp, $sp 4
      jr $ra

Largest: move $v0,$a1 # $v0 <- $a1
         blt $a0,$a1,Exit # if a0 >= $a1
         move $v0,$a0   # then $v0 <- $a0
Exit:    jr $ra
