.data 0x10000000
msg1: .asciiz "Please enter an  integer number:"


.text
.globl main
main: li $v0,4 # system call to print_str
      la $a0,msg1 # load address of msg1
      syscall

      li $v0, 5 # read integer
      syscall

      addu $t0, $v0, $0 # move input to $t0

      bltz $t0, main # if the input is negative, let user re-input
      move $a0,$t0
      addu $sp, $sp ,-4 # push $ra in stack
      sw $ra, 4($sp)
      jal Factorial # call Factorial 
      move $t0, $v0 # move returned value to $t0

      lw $ra, 4($sp) # restore $ra
      addu $sp, $sp ,4

      li $v0, 1 # print a integer
      add $a0, $t0, $0
      syscall
      jr $ra

Factorial:  subu $sp, $sp, 4 
            sw $ra, 4($sp)  # save the return address on stack

            beqz $a0, terminate # test for termination 
            subu $sp, $sp, 4 # do not terminate yet 
            sw $a0, 4($sp) # save the parameter 
            sub $a0, $a0, 1 # will call with a smaller argument 
            jal Factorial # after the termination condition is reached these lines 
                          # will be executed 
            lw $t0, 4($sp) # the argument I have saved on stack 
            mul $v0, $v0, $t0 # do the multiplication 
            lw $ra, 8($sp) # prepare to return 
            addu $sp, $sp, 8 # I’ve popped 2 words (an address and jr $ra # .. an argument)
            jr $ra

terminate:  li $v0, 1 
            lw $ra, 4($sp) 
            addu $sp, $sp, 4 
            jr $ra # 0! = 1 is the return value # get the return address # adjust the stack pointer # return