.data 0x10000000
msg1: .asciiz "Please enter first integer number:"
msg2: .asciiz "Please enter second integer number:"
msg3: .asciiz "Please enter two NON-negative numbers! \n"


.text
.globl main
main: li $v0,4 # system call to print_str
      la $a0,msg1 # load address of msg1
      syscall

      li $v0, 5 # read integer
      syscall

      addu $t0, $v0, $0 # move input to $t0

      li $v0,4 # system call to print_str
      la $a0,msg2 # load address of msg2
      syscall

      li $v0, 5 # read integer
      syscall

      addu $t1, $v0, $0 # move input to $t0
      
      slti $t2,$t0,0     # check if the first integer is non-negative
      bnez $t2,Warning
      slti $t2,$t1,0     # check if the second integer is non-negative
      bnez $t2, Warning

      move $a0,$t0 # pass the parameters
      move $a1,$t1

      addu $sp, $sp ,-4 # push $ra in stack
      sw $ra, 4($sp)
      jal Ackermann
      move $t0, $v0 # move returned value to $t0
      lw $ra, 4($sp) # restore $ra
      addu $sp, $sp ,4
      li $v0, 1 # print a integer
      add $a0, $t0, $0
      syscall
      jr $ra

Ackermann:  move $t0,$a0 # move parameters to $t0 and $t1 
            move $t1,$a1
            

            beqz $t0, Terminate # if x=0, terminate
            beqz $t1, Branch # if y=0, goto Branch

            addu $sp, $sp, -8 # puch $ra to stack
            sw $ra, 8($sp)
            sw $a0,4($sp) # push x in stack
            
            sub $a1,$a1,1 # y = y-1
            jal Ackermann # recursive
            move $a1,$v0 # move returned value to $a1
            lw $a0, 4($sp) # restore $a0
            lw $ra,8($sp) # restore $ra
            addu $sp,$sp,8 
            addu $sp, $sp, -4 # puch $ra to stack again
            sw $ra, 4($sp)
            sub $a0,$a0,1 # x = x-1
            jal Ackermann
            lw $ra, 4($sp) # restore $ra
            addu $sp, $sp ,4
            jr $ra



Terminate:  add $t0,$a1,1
            move $v0,$t0
            jr $ra

Branch:     addu $sp, $sp, -4 # puch $ra to stack
            sw $ra, 4($sp)

            sub $a0,$a0,1
            li $a1,1
            jal Ackermann
            lw $ra, 4($sp) # restore $ra
            addu $sp, $sp ,4
            jr $ra


Warning:    li $v0,4      # system call to print_str
            la $a0,msg3   #  load address of msg3 (warning message)
            syscall
            j main