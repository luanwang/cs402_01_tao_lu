.data 0x10000000
msg1: .asciiz "Please enter an integer number:"
msg2: .asciiz "The largest integer is: "

.text
.globl main
main: li $v0,4 # system call to print_str
      la $a0,msg1 # load address of msg1
      syscall

      li $v0, 5 # read integer
      syscall

      addu $t0, $v0, $0 # move input to $t0

      # read the second integer
      li $v0,4 # system call to print_str
      la $a0,msg1 # load address of msg1
      syscall

      li $v0, 5 # read integer
      syscall

      addu $t1, $v0, $0 # move input to $t1

      addu $a0, $t0, $0 # pass the first parameter to $s0 
      addu $a1, $t1, $0 # pass the second parameter to $s1

      add $sp, $sp, -12 
      sw $ra, 8($sp) # push $ra in stack
      sw $t0,4($sp)  # push paramater1 in stack
      sw $t1,0($sp)  # push parameter2 in stack

      jal Largest
      lw $t3, 4($sp) # pop return value
      li $v0, 4 
      la $a0, msg2
      syscall

      li $v0, 1 # print a integer
      add $a0, $t3, $0
      syscall

      lw $ra, 8($sp) # restore $ra
      add $sp, $sp 4
      jr $ra

Largest: lw $t0, 4($sp) # pop parameter1
         lw $t1, 0($sp) # pop parameter2
         blt $t1,$t0,Exit # if t0 < $t1
         sw $t1,4($sp)  # then push $t
Exit:    jr $ra
