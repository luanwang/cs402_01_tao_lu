#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "readfile.h"

#define MAXNAME 64
#define MAXPERSON 1024

struct person
{
    char first_name[MAXNAME], last_name[MAXNAME];
    int id, salary;
};

int cmpfunc(const void *a, const void *b)
{
    return ((struct person *)a)->id - ((struct person *)b)->id;
}

int binarysearch(struct person employee[],int l, int r, int x){
    if (r>=l){
        int mid = (r+l)/2;
        if (employee[mid].id == x) {
            return mid;
        } 
        if (employee[mid].id > x){
            return binarysearch(employee,l,mid-1,x);
        } else{
            return binarysearch(employee,mid+1,r,x);
        }
    }
    else{
        return -1;
    }
}
int main(int argc, char *argv[])
{
    // check if the argument is missed
    if (argc < 2)
    {
        printf("one argument expected. \n");
        return 0;
    }
    // declare some variables
    FILE *fp;
    // printf("%p",fp);
    char *file_name = argv[1];
    struct person employee[MAXPERSON];
    int emp_i = 0, choice;

    // read and close file
    if (open_file(file_name) == -1)
    {
        printf("File not exist \n");
        return 0;
    }
    else
    {
        fp = fopen(file_name, "r");
    }
    while (!feof(fp))
    {
        fscanf(fp, "%d %s %s %d", &employee[emp_i].id, employee[emp_i].first_name, employee[emp_i].last_name, &employee[emp_i].salary);
        emp_i++;
    }
    close_file(fp);

    while (1)
    {
        // print menu
        printf("%s\n", "Employee DB Menu:");
        printf("%s\n", "----------------------------------");
        printf("%s\n", "  (1) Print the Database");
        printf("%s\n", "  (2) Lookup by ID");
        printf("%s\n", "  (3) Lookup by Last Name");
        printf("%s\n", "  (4) Add an Employee");
        printf("%s\n", "  (5) Quit");
        printf("%s", "Enter your choice: ");

        if (read_int(&choice) == -1)
        {
            printf("EOF! \n");
            return 0;
        }

        while (choice < 1 || choice > 5)
        {
            printf("Hey, %d is not between 1 and 5, try again...\n", choice);
            printf("%s", "Enter your choice: ");
            int ret = read_int(&choice);
            if (ret == -1)
            {
                printf("EOF! \n");
                return 0;
            }
        }

        if (choice == 1)
        {
            // sort by ID
            qsort(&employee, emp_i, sizeof(struct person), cmpfunc);
            //print out
            printf("NAME                       SALARY 	 ID\n");
            printf("---------------------------------------------------------------\n");
            for (int i = 0; i < emp_i; i++)
            {
                printf("%-10s %-10s %10d %10d\n", employee[i].first_name, employee[i].last_name, employee[i].salary, employee[i].id);
            }
            printf("---------------------------------------------------------------\n");
            printf(" Number of Employees (%d)\n", emp_i);
            printf("\n");
        }
        else if (choice == 2)
        {
            // read employee id from input
            int in_id;
            printf("Enter a 6 digit employee id: ");
            if (read_int(&in_id) == -1)
            {
                return 0;
            }
            //search employee by usinng binary search
            printf("\n");
            int employee_id = binarysearch(employee, 0, emp_i, in_id);
            if (employee_id == -1)
            {
                printf("No employee with id: %d found \n", in_id);
            }
            else
            {
                printf("NAME                              SALARY 	     ID\n");
                printf("---------------------------------------------------------------\n");
                printf("%-10s %-10s %10d %10d\n", employee[employee_id].first_name, employee[employee_id].last_name, employee[employee_id].salary, employee[employee_id].id);
                printf("---------------------------------------------------------------\n");
            }
        }
        else if (choice == 3)
        {
            char in_str[MAXNAME];
            printf("Enter Employee's last name (no extra spaces): ");
            if (read_string(in_str) == -1){
                return 0;
            }

            int found = 0;
            printf("\n");
            for(int i=0; i<emp_i;i++){
                if (strcmp(in_str,employee[i].last_name) == 0){
                    printf("NAME                              SALARY 	     ID\n");
                    printf("---------------------------------------------------------------\n");
                    printf("%-10s %-10s %10d %10d\n", employee[i].first_name, employee[i].last_name, employee[i].salary, employee[i].id);
                    printf("---------------------------------------------------------------\n");
                    found = 1;
                    break;
                } 
            }   
            if (!found){
                printf("No employee with last name %s found\n\n",in_str);
            }          
        }
        else if (choice == 4)
        {
            struct person new_employee;
            while (1){
                printf("Enter the first name of the employee: ");
                int res = read_string((&new_employee)->first_name);
                if (res > 0){
                    break;
                }
            }
            while (1){
                printf("Enter the last name of the employee: ");
                int res = read_string((&new_employee)->last_name);
                if (res > 0){
                    break;
                }
            }
            while (1){
                printf("Enter employee's salary (30000 to 150000): ");
                int res = read_int(&new_employee.salary);
                if (res > 0 && new_employee.salary >=30000 && new_employee.salary <= 150000){
                    break;
                }
            }
            printf("do you want to add the following employee to the DB?\n");
            printf("       %s,  %s,  salary:  %d\n",new_employee.first_name,new_employee.last_name, new_employee.salary);
            while (1)
            {
                printf("Enter 1 for yes, 0 for no: ");
                int this_choice;
                read_int(&this_choice);
                if (this_choice == 1){
                    new_employee.id = employee[emp_i-1].id+1;
                    employee[emp_i] = new_employee;
                    emp_i++;
                    printf("One employee added\n\n");
                    break;
                }
                else if(this_choice == 0){
                    break;
                }
            }
        }
        else if (choice == 5)
        {
            printf("Thank you and goodbye!\n");
            return 0;
        }
    }
    return 0;
}
