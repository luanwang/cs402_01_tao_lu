.data 0x10010000
var1: .word 3 # initial var1
var2: .word 1 # initial var2
var3: .word -2020 # initial var3

.text
.globl main
main: 
    addu $s0,$ra, $0

    lw $t0,var1  # $t0 <- var1
    lw $t1, var2 # $t1 <- var2
    lw $t2, var3 # $t2 <- var3
    sle $t4,$t0,$t1 # if ($t0<$1) $t4=1; else $t4=0
    bgtz $t4,Else # if $t4 >0 jump to Else 
    sw $t2,var1  # save $t2 to var1
    sw $t2, var2 # save $t2 to var2
    beq $0,$0,Exit # exit the condition 

Else: 
    move $t3, $t0 #swap $t0 and $t1
    move $t0, $t1
    move $t1, $t3
    sw $t0, var1 
    sw $t1, var2

Exit:
    li $v0,1 # print var1
    lw $a0,var1
    syscall
    li $v0,1 # print var2
    lw $a0,var2
    syscall
    jr $ra