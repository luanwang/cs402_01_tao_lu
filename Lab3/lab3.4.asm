.data 0x10010000
my_array: .space 40 # initial my_array
var_j: .word 3

.text
.globl main
main: 
    addu $s0,$ra, $0
    li $t0,0 # $t0<-0 initial index
    li $a1,10 # $a1 <- 10 (limit)
    la $t1,my_array # load address of my_array
    lw $t2,var_j # $t2 <- j
Loop:    
    ble $a1, $t0,Exit # for loop
    sw $t2,0($t1) # my_array[i] <- j
    addi $t2,1 # j++
    addi $t1, 4 # next element of my_array
    addi $t0,$t0,1 # i++
    j Loop

Exit:
    jr $ra