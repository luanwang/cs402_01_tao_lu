.data 0x10010000
var1: .word 3 # initial var1

.text
.globl main
main: 
    addu $s0,$ra, $0
    lw $a0,var1  # $a0 <- var1 load var1 to $a0
    move $t0,$a0 # $t0 <- $a0 
    lw $t1, var1 # $t1 <- var1
    li $a1,100 # $a1 <- 100 (limit)
Loop:    
    ble $a1, $t0,Exit
    addi $t1,$t1,1
    addi $t0,$t0,1
    j Loop

Exit:
    sw $t1, var1
    li $v0,1 # print var1
    lw $a0,var1
    syscall
    jr $ra