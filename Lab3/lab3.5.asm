.data 0x10010000
msg_1: .asciiz "I'm far away" # message for lab far
msg_2: .asciiz "I'm nearby" # message for near lab
msg_3: .asciiz "please enter first integer: "
msg_4: .asciiz "please enter second integer: "

.text
.globl main
main: 
    addu $s0,$ra, $0
    li $v0, 4 # system call for print_str 
    la $a0, msg_3 # address of string to print
    syscall
    li $v0, 5 # read first integer
    syscall
    move $t0, $v0
    li $v0, 4 # system call for print_str 
    la $a0, msg_4 # address of string to print
    syscall
    li $v0, 5 # read second integer
    syscall
    move $t1, $v0
    beq $t0,$t1,Far # if ($t0 == t1), then go to Far
    li $v0,4 # print text
    la $a0,msg_2
    syscall
    jr $ra
Far: 
    li $v0,4 # print text
    la $a0,msg_1
    syscall
    jr $ra