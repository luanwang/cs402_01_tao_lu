#include <stdio.h>
#include <stdlib.h>
#include  <string.h>
#include <math.h>

#define TRADEOFF 2 // tradeoff of the array

// compare function for qsort()
int compfun(const void *a, const void *b){
    return (*(float *)a-*(float *)b);
}
// function of read data from file and store it to array
// The first two elements of the array are reserved for the meta data: size and unused capacity
// the data begins from 3. element
float * readdata(FILE * fp){
    int array_size = 20; //initial the array size;
    int numberValue = 0; // the size of the array
    int unusedCap = 20; // unsed capacity
    float * ptr = (float *) malloc(array_size*sizeof(float)+TRADEOFF); // initialize the array
    int idx = TRADEOFF; // index of the data 
    while (!feof(fp))
    {
        if (unusedCap == 0){
            unusedCap += array_size;
            array_size *=2;
            float * temp_ptr = (float *) malloc(array_size*sizeof(float)+TRADEOFF);
            // copy the old array to new array
            for (int i=0; i<numberValue+TRADEOFF; i++){
                temp_ptr[i] = ptr[i];
            }
            // free the old array
            free(ptr);
            // repoint the old array pointer to new one
            ptr = temp_ptr;
        } 
        numberValue++;
        fscanf(fp,"%f\n",&ptr[idx]);  
        unusedCap--; 
        idx++;

    }
    // store the meta data
    ptr[0] = numberValue;
    ptr[1] = unusedCap;
    return ptr;

}

//calculate the mean
double calmean(float array[]){
    int numberValue = (int)array[0];
    float sum = 0;
    for (int i=0; i<numberValue;i++){
        sum+=array[i+2];
    }
    return (double)sum/numberValue; 
}

// calculate the standard deviation
double calstddev(float array[]){
    int numberValue = (int)array[0];
    double mean = calmean(array);
    double square_sum = 0;
    for (int i=0; i<numberValue; i++){
        square_sum += pow((array[i+2]-mean),2);
    }
    double stddev = sqrt(square_sum/numberValue);
    return stddev;
}

//calculate the median
float calmedian(float array[]){
    int numberValue = array[0];
    float tradeoff_array[numberValue];
    for (int i=0; i<numberValue; i++){
        tradeoff_array[i] = array[i+TRADEOFF];
    }
    qsort(tradeoff_array,numberValue,sizeof(float),compfun);

    // if the length is odd
    if (numberValue%2 == 1){
        return tradeoff_array[numberValue/2];
    } else{
        // if the length is even
        return (tradeoff_array[numberValue/2-1]+tradeoff_array[numberValue/2])/2;
    }
}

int main(int argc, char const *argv[])
{
    // read the file     
    FILE *fp;
    fp = fopen(argv[1],"r");
    
    float * mydata = readdata(fp);
    fclose(fp);
 
    printf(" Results:\n");  
    printf("--------\n");
    printf("%10s :      %-50d\n","Num values",(int)mydata[0]);
    printf("%10s :      %-50.3f\n","mean",calmean(mydata));
    printf("%10s :      %-50.3f\n","median",calmedian(mydata));
    printf("%10s :      %-50.3f\n","stddev",calstddev(mydata));
    printf("%10s :      %-50d\n","Unused array capacity",(int)mydata[1]);
    
    return 0;
}
  