#include <stdio.h>

int open_file(char *file_name){
    FILE *fp;
    fp = fopen(file_name,"r");
    if (fp){
        return 0;
    } else {
        return -1;
    }
}

int close_file(FILE *fp){
    return fclose(fp);
}

int read_int(int *in){
    return scanf("%d",in);
}

int read_string(char *in){
    return scanf("%s",in);
}

int read_float(float *in){
    return scanf("%e",in);
}