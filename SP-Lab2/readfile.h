int open_file(char *file_name);
int close_file();
int read_int(int *in);
int read_string(char *in);
int read_float(float *in);