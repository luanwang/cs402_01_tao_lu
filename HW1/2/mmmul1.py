import random
import time


def mul(x, y):
    """
    matrix multiplication
    :param x: matrix x
    :param y: matrix y
    :return: x * y
    """
    x_row = len(x)
    x_col = len(x[0])
    y_row = len(y)
    y_col = len(y[0])
    if x_col != y_row:
        raise ValueError("range error")

    # create a zero result matrix
    result = [[0] * y_col for _ in range(x_row)]

    # multiplication
    for i in range(x_row):
        # iterate through columns of Y
        for j in range(y_col):
            # iterate through rows of Y
            for k in range(x_col):
                result[i][j] += x[i][k] * y[k][j]
    return result


def create_random_matrix(row, col, is_int=True):
    """
    create a random matrix
    :param row: row number
    :param col: column number
    :param is_int: if all the elements are integer
    :return: a random matrix (row x col)
    """
    result = [[0] * col for _ in range(row)]
    for i in range(row):
        for j in range(col):
            if is_int:
                result[i][j] = random.randrange(1, 50)
            else:
                result[i][j] = random.random()
    return result


if __name__ == '__main__':
    time_record = []  # record the time consume
    # Test 1 (all elements are integer)
    test_number = 10
    x_row_num = 200
    x_col_num = 250
    y_row_num = 250
    y_col_num = 300
    for _ in range(test_number):
        X = create_random_matrix(x_row_num, x_col_num)
        Y = create_random_matrix(y_row_num, y_col_num)
        star_time = time.time()
        mul(X, Y)
        time_record.append(time.time() - star_time)
    print(
        f"Test for integer elements: \n Time record is {time_record}, \n the average time elapsed is {sum(time_record) / len(time_record)} seconds")
    # Test 2 (all elements are real numbers)
    test_number = 10
    x_row_num = 200
    x_col_num = 250
    y_row_num = 250
    y_col_num = 300
    for _ in range(test_number):
        X = create_random_matrix(x_row_num, x_col_num, is_int=False)
        Y = create_random_matrix(y_row_num, y_col_num, is_int=False)
        star_time = time.time()
        mul(X, Y)
        time_record.append(time.time() - star_time)
    print(
        f"Test for real numbers: \n Time record is {time_record}, \n the average time elapsed is {sum(time_record) / len(time_record)} seconds")
