.data 0x10010000 
var1: .word 31  # initial var1 
var2: .word 11 # initial var2
.extern ext1 4 # initial ext1 in extern with 32-bit
.extern ext2 4 # initial ext2 in extern with 32-bit

.text 
.globl main 
main: addu $s0, $ra, $0  # save $31 in $16

lw $t0, var1 # load word from var1 in $t0
lw $t1, var2 # load word from var2 in $t1
sw $t0, ext1 # save value from $t0 to ext1
sw $t1, ext2 # save value from $t1 to ext2

# restore now the return address in $ra and return from main
addu $ra, $0, $s0 # return address back in $31 
jr $ra # return from main