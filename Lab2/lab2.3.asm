.data 0x10010000 
var1: .word 83  # initial var1 
var2: .word 104
var3: .word 111
var4: .word 119
first: .byte 'l'
last: .byte 't'

.text 
.globl main 
main: addu $s0, $ra, $0  # save $31 in $16
lui $1, 4097              
lw $8, 0($1)
lui $1, 4097             
lw $9, 4($1)             
lui $1, 4097             
lw $10, 8($1)            
lui $1, 4097             
lw $11, 12($1)           
lui $1, 4097             
sw $11, 0($1)            
lui $1, 4097             
sw $8, 12($1)            
lui $1, 4097             
sw $9, 8($1)             
lui $1, 4097             
sw $10, 4($1) 

# restore now the return address in $ra and return from main
addu $ra, $0, $s0 # return address back in $31 
jr $ra # return from main