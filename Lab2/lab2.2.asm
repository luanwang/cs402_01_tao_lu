.data 0x10010000 
var1: .word 83  # initial var1 
var2: .word 104 # initial var2
var3: .word 111 # initial var3
var4: .word 119 # initial var4
first: .byte 'l' # initial first
last: .byte 't' # initial last

.text 
.globl main 
main: addu $s0, $ra, $0  # save $31 in $16

lw $t0, var1 # load var1 in $t0
lw $t1, var2 # load var2 in $t1 
lw $t2, var3 # load var3 in $t2
lw $t3, var4 # load var4 in $t3
sw $t3, var1 # store the value in $t3 to the var1 address
sw $t0, var4 # store the value in $t0 to the var4 address
sw $t1, var3 # store the value in $t1 to the var3 address
sw $t2, var2 # store the value in $t2 to the var2 address

# restore now the return address in $ra and return from main
addu $ra, $0, $s0 # return address back in $31 
jr $ra # return from main